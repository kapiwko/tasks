<?php

declare(strict_types=1);

namespace App\Tests;

use DateInterval;
use DateTimeImmutable;

class Date
{
    public static function createFromString(string $date): DateTimeImmutable
    {
        return DateTimeImmutable::createFromFormat('Y-m-d', $date)->setTime(0, 0, 0);
    }

    public static function createRelativeToToday(int $days): DateTimeImmutable
    {
        $date = (new DateTimeImmutable())->setTime(0, 0, 0);

        if (0 === $days) {
            return $date;
        }

        $abs = abs($days);
        $interval = new DateInterval("P${abs}D");

        return $days > 0 ? $date->add($interval) : $date->sub($interval);
    }
}
