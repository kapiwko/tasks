<?php

declare(strict_types=1);

namespace App\Tests\Infrastructure\DateRangeValue;

use App\Infrastructure\DateRangeValue\DateRangeValue;
use App\Infrastructure\DateRangeValue\DateRangeValueInterface;
use App\Tests\Date;
use PHPUnit\Framework\TestCase;

/**
 * @covers \App\Infrastructure\DateRangeValue\DateRangeValue
 */
class DateRangeValueTest extends TestCase
{
    public function test(): void
    {
        $from = Date::createFromString('2017-01-02');
        $to = Date::createFromString('2017-02-12');
        $dateRangeValue = new DateRangeValue(100, $from, $to);
        self::assertInstanceOf(DateRangeValueInterface::class, $dateRangeValue);
        self::assertEquals(100, $dateRangeValue->value());
        self::assertEquals('2017-01-02', $dateRangeValue->from()->format('Y-m-d'));
        self::assertEquals('2017-02-12', $dateRangeValue->to()->format('Y-m-d'));
    }
}
