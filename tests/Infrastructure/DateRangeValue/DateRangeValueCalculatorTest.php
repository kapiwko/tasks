<?php

declare(strict_types=1);

namespace App\Tests\Infrastructure\DateRangeValue;

use App\Infrastructure\DateRangeValue\DateRangeValue;
use App\Infrastructure\DateRangeValue\DateRangeValueCalculator;
use App\Infrastructure\DateRangeValue\DateRangeValueCalculatorInterface;
use App\Tests\Date;
use PHPUnit\Framework\TestCase;
use RuntimeException;

/**
 * @covers \App\Infrastructure\DateRangeValue\DateRangeValueCalculator
 */
class DateRangeValueCalculatorTest extends TestCase
{
    public function testGetForRanges(): void
    {
        $calculator = new DateRangeValueCalculator();
        self::assertInstanceOf(DateRangeValueCalculatorInterface::class, $calculator);
        $calculator->add(new DateRangeValue(100, Date::createFromString('2018-01-09'), Date::createFromString('2018-01-11')));
        $calculator->add(new DateRangeValue(200, Date::createFromString('2018-01-13'), Date::createFromString('2018-01-15')));
        $calculator->add(new DateRangeValue(200, Date::createFromString('2018-02-13'), Date::createFromString('2018-02-15')));
        self::assertEquals(0, $calculator->get(Date::createFromString('2018-01-08')));
        self::assertEquals(100, $calculator->get(Date::createFromString('2018-01-09')));
        self::assertEquals(100, $calculator->get(Date::createFromString('2018-01-10')));
        self::assertEquals(100, $calculator->get(Date::createFromString('2018-01-11')));
        self::assertEquals(0, $calculator->get(Date::createFromString('2018-01-12')));
        self::assertEquals(200, $calculator->get(Date::createFromString('2018-01-13')));
        self::assertEquals(200, $calculator->get(Date::createFromString('2018-01-14')));
        self::assertEquals(200, $calculator->get(Date::createFromString('2018-01-15')));
        self::assertEquals(0, $calculator->get(Date::createFromString('2018-01-16')));
        $calculator->add(new DateRangeValue(300, Date::createFromString('2018-01-10'), Date::createFromString('2018-01-14')));
        self::assertEquals(0, $calculator->get(Date::createFromString('2018-01-08')));
        self::assertEquals(100, $calculator->get(Date::createFromString('2018-01-09')));
        self::assertEquals(400, $calculator->get(Date::createFromString('2018-01-10')));
        self::assertEquals(400, $calculator->get(Date::createFromString('2018-01-11')));
        self::assertEquals(300, $calculator->get(Date::createFromString('2018-01-12')));
        self::assertEquals(500, $calculator->get(Date::createFromString('2018-01-13')));
        self::assertEquals(500, $calculator->get(Date::createFromString('2018-01-14')));
        self::assertEquals(200, $calculator->get(Date::createFromString('2018-01-15')));
        self::assertEquals(0, $calculator->get(Date::createFromString('2018-01-16')));
        self::assertEquals(0, $calculator->get(Date::createFromString('2018-02-12')));
    }

    public function testGetForInfinities(): void
    {
        $calculator = new DateRangeValueCalculator();
        $calculator->add(new DateRangeValue(100, Date::createFromString('2018-01-09'), null));
        $calculator->add(new DateRangeValue(200, null, Date::createFromString('2018-01-11')));
        self::assertEquals(200, $calculator->get(Date::createFromString('2018-01-08')));
        self::assertEquals(300, $calculator->get(Date::createFromString('2018-01-09')));
        self::assertEquals(300, $calculator->get(Date::createFromString('2018-01-10')));
        self::assertEquals(300, $calculator->get(Date::createFromString('2018-01-11')));
        self::assertEquals(100, $calculator->get(Date::createFromString('2018-01-12')));
        $calculator->add(new DateRangeValue(300, null, null));
        self::assertEquals(500, $calculator->get(Date::createFromString('2018-01-08')));
        self::assertEquals(600, $calculator->get(Date::createFromString('2018-01-09')));
        self::assertEquals(600, $calculator->get(Date::createFromString('2018-01-10')));
        self::assertEquals(600, $calculator->get(Date::createFromString('2018-01-11')));
        self::assertEquals(400, $calculator->get(Date::createFromString('2018-01-12')));
    }

    public function testGetValues(): void
    {
        $calculator = new DateRangeValueCalculator();
        $calculator->add(new DateRangeValue(100, Date::createFromString('2018-01-09'), Date::createFromString('2018-01-11')));
        $calculator->add(new DateRangeValue(200, Date::createFromString('2018-01-10'), Date::createFromString('2018-01-14')));
        $calculator->add(new DateRangeValue(300, Date::createFromString('2018-01-13'), Date::createFromString('2018-01-15')));
        self::assertEquals([
            '2018-01-08' => 0,
            '2018-01-09' => 100,
            '2018-01-10' => 300,
            '2018-01-11' => 300,
            '2018-01-12' => 200,
            '2018-01-13' => 500,
            '2018-01-14' => 500,
            '2018-01-15' => 300,
            '2018-01-16' => 0,
        ], $calculator->getRange(Date::createFromString('2018-01-08'), Date::createFromString('2018-01-16')));
    }

    public function testTags(): void
    {
        $calculator = new DateRangeValueCalculator();
        $calculator->add(new DateRangeValue(100, Date::createFromString('2018-01-02'), Date::createFromString('2018-01-04')), ['a', 'b']);
        $calculator->add(new DateRangeValue(100, Date::createFromString('2018-01-03'), Date::createFromString('2018-01-05')), ['a']);
        $calculator->add(new DateRangeValue(100, Date::createFromString('2018-01-04'), Date::createFromString('2018-01-06')), ['b']);
        $calculator->add(new DateRangeValue(100, Date::createFromString('2018-01-05'), Date::createFromString('2018-01-07')), ['a']);
        $calculator->add(new DateRangeValue(100, Date::createFromString('2018-01-06'), Date::createFromString('2018-01-08')), ['a', 'b']);
        self::assertEquals([
            '2018-01-01' => 0,
            '2018-01-02' => 100,
            '2018-01-03' => 200,
            '2018-01-04' => 300,
            '2018-01-05' => 300,
            '2018-01-06' => 300,
            '2018-01-07' => 200,
            '2018-01-08' => 100,
            '2018-01-09' => 0,
        ], $calculator->getRange(Date::createFromString('2018-01-01'), Date::createFromString('2018-01-09')));
        self::assertEquals([
            '2018-01-01' => 0,
            '2018-01-02' => 100,
            '2018-01-03' => 200,
            '2018-01-04' => 200,
            '2018-01-05' => 200,
            '2018-01-06' => 200,
            '2018-01-07' => 200,
            '2018-01-08' => 100,
            '2018-01-09' => 0,
        ], $calculator->getRange(Date::createFromString('2018-01-01'), Date::createFromString('2018-01-09'), 'a'));
        self::assertEquals([
            '2018-01-01' => 0,
            '2018-01-02' => 100,
            '2018-01-03' => 100,
            '2018-01-04' => 200,
            '2018-01-05' => 100,
            '2018-01-06' => 200,
            '2018-01-07' => 100,
            '2018-01-08' => 100,
            '2018-01-09' => 0,
        ], $calculator->getRange(Date::createFromString('2018-01-01'), Date::createFromString('2018-01-09'), 'b'));
    }

    public function testGetMaxMin(): void
    {
        $calculator = new DateRangeValueCalculator();
        $calculator->add(new DateRangeValue(100, Date::createFromString('2018-01-09'), Date::createFromString('2018-01-11')));
        $calculator->add(new DateRangeValue(200, Date::createFromString('2018-01-10'), Date::createFromString('2018-01-14')));
        $calculator->add(new DateRangeValue(300, Date::createFromString('2018-01-13'), Date::createFromString('2018-01-15')));
        self::assertEquals(500, $calculator->getMax(Date::createFromString('2018-01-08'), Date::createFromString('2018-01-16')));
        self::assertEquals(0, $calculator->getMin(Date::createFromString('2018-01-08'), Date::createFromString('2018-01-16')));
    }

    public function testGetRangeWrongDates(): void
    {
        $calculator = new DateRangeValueCalculator();
        $this->expectException(RuntimeException::class);
        $calculator->getRange(Date::createFromString('2018-01-08'), Date::createFromString('2018-01-07'));
    }

    public function testGetMaxWrongDates(): void
    {
        $calculator = new DateRangeValueCalculator();
        $this->expectException(RuntimeException::class);
        $calculator->getMax(Date::createFromString('2018-01-08'), Date::createFromString('2018-01-07'));
    }

    public function testGetMinWrongDates(): void
    {
        $calculator = new DateRangeValueCalculator();
        $this->expectException(RuntimeException::class);
        $calculator->getMin(Date::createFromString('2018-01-08'), Date::createFromString('2018-01-07'));
    }
}
