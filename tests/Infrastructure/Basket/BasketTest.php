<?php

declare(strict_types=1);

namespace App\Tests\Infrastructure\Basket;

use App\Infrastructure\Basket\Basket;
use App\Infrastructure\Basket\BasketProductItem;
use App\Infrastructure\Basket\BasketDiscountItem;
use App\Infrastructure\Basket\FileBasketRepository;
use App\Infrastructure\Discount\AmountDiscount;
use App\Infrastructure\Discount\DiscountRepository;
use App\Infrastructure\Discount\PercentDiscount;
use App\Infrastructure\Discount\DiscountForCategory;
use App\Infrastructure\Discount\DiscountWithLimitations;
use App\Infrastructure\Product\Product;
use App\Infrastructure\Product\ProductRepository;
use App\Infrastructure\Product\PromotionalProduct;
use PHPUnit\Framework\TestCase;

/**
 * @covers \App\Infrastructure\Basket\Basket
 */
class BasketTest extends TestCase
{
    public function test(): void
    {
        $basket = new Basket('Koszyk 1');
        self::assertEquals(0, $basket->qty());
        self::assertEquals(0, $basket->value());

        $productRepository = new ProductRepository();

        $product1 = $productRepository->get('MA1');
        $product2 = $productRepository->get('CH1');

        $basket->addProduct($product1, 3);
        self::assertEquals(1500, $basket->value());
        self::assertEquals(1500, $basket->productsValue());
        self::assertEquals(1, $basket->qty());


        $basket->removeProduct($product1, 2);
        self::assertEquals(500, $basket->value());
        self::assertEquals(500, $basket->productsValue());
        self::assertEquals(1, $basket->qty());

        $basket->removeProduct($product1, 1);
        self::assertEquals(0, $basket->value());
        self::assertEquals(0, $basket->productsValue());
        self::assertEquals(0, $basket->qty());

        $basket->addProduct($product2, 3);
        self::assertEquals(750, $basket->value());
        self::assertEquals(750, $basket->productsValue());
        self::assertEquals(1, $basket->qty());

        $basket->addProduct($product1, 2);
        self::assertEquals(1000+750, $basket->value());
        self::assertEquals(1000+750, $basket->productsValue());
        self::assertEquals(2, $basket->qty());

        $basket->removeProduct($product1, 2);
        $basket->removeProduct($product2, 3);
        self::assertEquals(0, $basket->value());
        self::assertEquals(0, $basket->productsValue());
        self::assertEquals(0, $basket->qty());

        // -----------------------------------------------------------------------
        // PromotionalProduct = n-ty produkt za 1 grosz
        $promotionalProduct1 = $productRepository->get('ML2');
        //$promotionalProduct1 = new PromotionalProduct($product3, 3, 1);


        $basket->addProduct($promotionalProduct1, 1);
        self::assertEquals(500, $basket->value());
        self::assertEquals(500, $basket->productsValue());
        self::assertEquals(1, $basket->qty());

        $basket->addProduct($promotionalProduct1, 1);
        self::assertEquals(1000+1, $basket->value());
        self::assertEquals(1000+1, $basket->productsValue());
        self::assertEquals(1, $basket->qty());

        $basket->removeProduct($promotionalProduct1, 1);
        self::assertEquals(500, $basket->value());
        self::assertEquals(500, $basket->productsValue());
        self::assertEquals(1, $basket->qty());

        $basket->addProduct($promotionalProduct1, 3);
        self::assertEquals(2000+2, $basket->value());
        self::assertEquals(2000+2, $basket->productsValue());
        self::assertEquals(1, $basket->qty());

        // -----------------------------------------------------------------------
        $discountRepository = new DiscountRepository();

        // AmountDiscount - zniżka kwotowa
        $amountDiscount1 = $discountRepository->get('Amount 1');
        $amountDiscount2 = $discountRepository->get('Amount 2');
        $amountDiscount3 = $discountRepository->get('Amount 3');

        $basket->addDiscount($amountDiscount2); // 1000
        self::assertEquals(1000+2, $basket->value());
        self::assertEquals(2000+2, $basket->productsValue());
        self::assertEquals(1, $basket->qty());

        $basket->removeDiscount($amountDiscount2); // 1000
        self::assertEquals(2000+2, $basket->value());
        self::assertEquals(2000+2, $basket->productsValue());
        self::assertEquals(1, $basket->qty());

        $basket->addDiscount($amountDiscount1); // 2001
        self::assertEquals(1, $basket->value());
        self::assertEquals(2000+2, $basket->productsValue());
        self::assertEquals(1, $basket->qty());

        $basket->addDiscount($amountDiscount3); // 10
        self::assertEquals(0, $basket->value());
        self::assertEquals(2000+2, $basket->productsValue());
        self::assertEquals(1, $basket->qty());

        $basket->removeDiscount($amountDiscount1);
        $basket->removeDiscount($amountDiscount3);
        self::assertEquals(2000+2, $basket->value());
        self::assertEquals(2000+2, $basket->productsValue());
        self::assertEquals(1, $basket->qty());

        $basket->removeProduct($promotionalProduct1, 4);
        self::assertEquals(0, $basket->value());
        self::assertEquals(0, $basket->productsValue());
        self::assertEquals(0, $basket->qty());

        // -----------------------------------------------------------------------
        // Discount - zniżka procentowa
        $percentDiscount1 = $discountRepository->get('Discount 1');
        $percentDiscount2 = $discountRepository->get('Discount 2');


        $basket->addProduct($product1, 2);
        self::assertEquals(1000, $basket->value());
        self::assertEquals(1000, $basket->productsValue());
        self::assertEquals(1, $basket->qty());

        $basket->addProduct($product2, 2);
        self::assertEquals(1500, $basket->value());
        self::assertEquals(1500, $basket->productsValue());
        self::assertEquals(2, $basket->qty());

        $basket->addDiscount($percentDiscount1);
        self::assertEquals(1500-15, $basket->value());
        self::assertEquals(1500, $basket->productsValue());
        self::assertEquals(2, $basket->qty());

        $basket->addDiscount($percentDiscount2);
        self::assertEquals(1500-15-150, $basket->value());
        self::assertEquals(1500, $basket->productsValue());
        self::assertEquals(2, $basket->qty());

        // -----------------------------------------------------------------------
        // DiscountWithLimitations - zniżka procentowa działająca tak jak opisałem ostanio ;)

        $discountWithLimitations1 = $discountRepository->get('Limitations 1');
        $discountWithLimitations2 = $discountRepository->get('Limitations 2');
        $discountWithLimitations3 = $discountRepository->get('Limitations 3');
        $discountWithLimitations4 = $discountRepository->get('Limitations 4');
        $discountWithLimitations5 = $discountRepository->get('Limitations 5');

        $basket->addDiscount($discountWithLimitations1);
        self::assertEquals(1500-15-150, $basket->value());
        self::assertEquals(1500, $basket->productsValue());
        self::assertEquals(2, $basket->qty());

        $basket->addDiscount($discountWithLimitations2);
        self::assertEquals(1500-15-150-1000, $basket->value());
        self::assertEquals(1500, $basket->productsValue());
        self::assertEquals(2, $basket->qty());

        $basket->removeDiscount($discountWithLimitations2);
        self::assertEquals(1500-15-150, $basket->value());
        self::assertEquals(1500, $basket->productsValue());
        self::assertEquals(2, $basket->qty());

        $basket->addDiscount($discountWithLimitations3);
        self::assertEquals(1500-15-150-15, $basket->value());
        self::assertEquals(1500, $basket->productsValue());
        self::assertEquals(2, $basket->qty());

        $basket->addDiscount($discountWithLimitations4);
        self::assertEquals(1500-15-150-15-12, $basket->value());
        self::assertEquals(1500, $basket->productsValue());
        self::assertEquals(2, $basket->qty());

        $basket->addDiscount($discountWithLimitations5);
        self::assertEquals(0, $basket->value());
        self::assertEquals(1500, $basket->productsValue());
        self::assertEquals(2, $basket->qty());

        $basket->removeDiscount($discountWithLimitations1);
        $basket->removeDiscount($discountWithLimitations3);
        $basket->removeDiscount($discountWithLimitations4);
        $basket->removeDiscount($discountWithLimitations5);
        self::assertEquals(1500-15-150, $basket->value());
        self::assertEquals(1500, $basket->productsValue());
        self::assertEquals(2, $basket->qty());

        // -----------------------------------------------------------------------
        // DiscountForMilk - zniżka na produkty mleczne
        $discountMilk1 = $discountRepository->get('Milk discount 1');  // 2001
        $discountMilk2 = $discountRepository->get('Milk discount 2');  // 1000
        $discountMilk3 = $discountRepository->get('Milk discount 3'); // 1%

        $basket->addDiscount($discountMilk1);
        self::assertEquals(1500-15-150-1000, $basket->value());
        self::assertEquals(1500, $basket->productsValue());
        self::assertEquals(2, $basket->qty());

        $basket->removeDiscount($discountMilk1);
        self::assertEquals(1500-15-150, $basket->value());
        self::assertEquals(1500, $basket->productsValue());
        self::assertEquals(2, $basket->qty());

        $basket->addDiscount($discountMilk2);
        self::assertEquals(1500-15-150-1000, $basket->value());
        self::assertEquals(1500, $basket->productsValue());
        self::assertEquals(2, $basket->qty());

        $basket->removeDiscount($discountMilk2);
        self::assertEquals(1500-15-150, $basket->value());
        self::assertEquals(1500, $basket->productsValue());
        self::assertEquals(2, $basket->qty());

        $basket->addDiscount($discountMilk3);
        self::assertEquals(1500-15-150-10, $basket->value());
        self::assertEquals(1500, $basket->productsValue());
        self::assertEquals(2, $basket->qty());

        // -----------------------------------------------------------------------
        // >>>> Wyczyszczenie koszyka
        $basket->clear();
        self::assertEquals(0, $basket->value());
        self::assertEquals(0, $basket->productsValue());
        self::assertEquals(0, $basket->qty());

        // -----------------------------------------------------------------------
        // >>>> Połączenie zniżek
        // Produkty
        $product1 = $productRepository->get('MA1');
        $product2 = $productRepository->get('CH1');
        $product3 = $productRepository->get('ML1');

        $basket->addProduct($product1, 4);  // 2000 (mleczne)
        $basket->addProduct($product2, 4);  // 1000 (pieczywo)
        $basket->addProduct($product3, 2);  // 600  (mleczne)
        self::assertEquals(3600, $basket->value());
        self::assertEquals(3600, $basket->productsValue());
        self::assertEquals(3, $basket->qty());


        $discountWithLimitations6 = $discountRepository->get('Limitations 6');
        $discountWithLimitations7 = $discountRepository->get('Limitations 7');
        $discountWithLimitations8 = $discountRepository->get('Limitations 8');

        $basket->addDiscount($discountWithLimitations6);
        self::assertEquals(3600, $basket->value());
        self::assertEquals(3600, $basket->productsValue());
        self::assertEquals(3, $basket->qty());

        $basket->removeDiscount($discountWithLimitations6);
        self::assertEquals(3600, $basket->value());
        self::assertEquals(3600, $basket->productsValue());
        self::assertEquals(3, $basket->qty());

        $basket->addDiscount($discountWithLimitations7);
        self::assertEquals(1000+2600-260, $basket->value());
        self::assertEquals(3600, $basket->productsValue());
        self::assertEquals(3, $basket->qty());

        $basket->removeDiscount($discountWithLimitations7);

        $basket->addDiscount($discountWithLimitations8);
        self::assertEquals(1000+2600-200, $basket->value());
        self::assertEquals(3600, $basket->productsValue());
        self::assertEquals(3, $basket->qty());

        // kolejny produkt

        $basket->addProduct($product1, 1);  // 500 (mleczne)
        self::assertEquals(1000+3100-200, $basket->value());
        self::assertEquals(4100, $basket->productsValue());
        self::assertEquals(3, $basket->qty());

        $basket->removeDiscount($discountWithLimitations8);

        $basket->addDiscount($discountWithLimitations7);
        $basket->addProduct($product1, 1);  // 500 (mleczne)

        $basket_id = $basket->id();
        $fileRepository = new FileBasketRepository($productRepository, $discountRepository);
        $fileRepository->add($basket);
        $basket2 = $fileRepository->get($basket_id);


        // var_dump($data2);
        // var_dump($items);
        // var_dump($basket->items());


        // var_dump($basket2);


        self::assertEquals(1000+3600-360, $basket2->value());
        self::assertEquals(4600, $basket2->productsValue());
        self::assertEquals(3, $basket2->qty());
        // -----------------------------------------------------------------------

        // Zapis produktów
        $cName1 = "Bułka kajzerka";
        $product01 = new Product("BU1", $cName1, 150, "pieczywo");
        if ($productRepository->isProduct("BU1")) {
            $productRepository->remove($product01);
        }
        $productRepository->add($product01);

        $cName2 = "Bułka duża";
        $product02 = new Product("BU2", $cName2, 200, "pieczywo");
        if ($productRepository->isProduct("BU2")) {
            $productRepository->remove($product02);
        }
        $promotionalProduct02 = new PromotionalProduct($product02, 4, 1);
        $productRepository->add($promotionalProduct02);

        $product01 = $productRepository->get('BU1');
        $product02 = $productRepository->get('BU2');

        self::assertEquals($cName1, $product01->name());
        self::assertEquals($cName2, $product02->name());

        $productRepository->commit();

        // -----------------------------------------------------------------------
        // Zapis zniżek
        $idAmountDiscount = "Amount 4";
        $amountDiscount = new AmountDiscount($idAmountDiscount, 125);
        if ($discountRepository->isDiscount($idAmountDiscount)) {
            $discountRepository->remove($amountDiscount);
        }
        $discountRepository->add($amountDiscount);

        $idPercentDiscount = "Discount 3";
        $percentDiscount = new PercentDiscount($idPercentDiscount, 50);
        if ($discountRepository->isDiscount($idPercentDiscount)) {
            $discountRepository->remove($percentDiscount);
        }
        $discountRepository->add($percentDiscount);

        $idCategoryDiscount = "Milk discount 5";
        $categoryDiscount = new DiscountForCategory($idCategoryDiscount, ["mleczne"], $amountDiscount);
        if ($discountRepository->isDiscount($idCategoryDiscount)) {
            $discountRepository->remove($categoryDiscount);
        }
        $discountRepository->add($categoryDiscount);

        $idLimitationsDiscount = "Limitations 9";
        $limitationsDiscount = new DiscountWithLimitations($idLimitationsDiscount, 7500, 1000, $amountDiscount);
        if ($discountRepository->isDiscount($idLimitationsDiscount)) {
            $discountRepository->remove($limitationsDiscount);
        }
        $discountRepository->add($limitationsDiscount);

        $amountDiscount = $discountRepository->get($idAmountDiscount);
        $percentDiscount = $discountRepository->get($idPercentDiscount);
        $categoryDiscount = $discountRepository->get($idCategoryDiscount);
        $limitationsDiscount = $discountRepository->get($idLimitationsDiscount);

        self::assertEquals($idAmountDiscount, $amountDiscount->id());
        self::assertEquals($idPercentDiscount, $percentDiscount->id());
        self::assertEquals($idCategoryDiscount, $categoryDiscount->id());
        self::assertEquals($idLimitationsDiscount, $limitationsDiscount->id());

        $discountRepository->commit();

        // -----------------------------------------------------------------------
        /*
        echo ' qty: ' . $basket->qty() . "\n";
        echo ' product value: ' . $basket->productsValue() . "\n";
        echo ' basket value: ' . $basket->value() . "\n";
        */



        //$repository = new FileBasketRepository();
        //$repository->add($basket);
    }
}
