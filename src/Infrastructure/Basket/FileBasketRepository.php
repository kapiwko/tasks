<?php

declare(strict_types=1);

namespace App\Infrastructure\Basket;


use App\Infrastructure\Discount\DiscountRepository;
use App\Infrastructure\Discount\DiscountRepositoryInterface;
use App\Infrastructure\Product\ProductRepository;
use App\Infrastructure\Product\ProductRepositoryInterface;

class FileBasketRepository implements BasketRepositoryInterface
{
    private ProductRepositoryInterface $productRepository;
    private DiscountRepositoryInterface $discountRepository;

    public function __construct(
        ProductRepositoryInterface $productRepository,
        DiscountRepositoryInterface $discountRepository
    )
    {
        $this->productRepository = $productRepository;
        $this->discountRepository = $discountRepository;
    }

    private static function fileName(string $id): string
    {
        return 'basket_' . $id;
    }

    public function get(string $id): BasketInterface
    {
        $fileName = self::fileName($id);
        $data = json_decode(file_get_contents($fileName), true);

        $basket = new Basket($data['id']);

        foreach ($data['items'] as $id => $item) {
            $id_item = $item['id'];

            switch ($item['type']) {
                case 'product':
                    $product = $this->productRepository->get($id_item);
                    $basket->addProduct($product, $item['qty']);
                    break;

                case 'discount':
                    $discount = $this->discountRepository->get($id_item);
                    $basket->addDiscount($discount);
            }
        }

        return $basket;
    }

    public function add(BasketInterface $basket): void
    {
        $fileName = self::fileName($basket->id());
        $data = json_encode($basket, JSON_THROW_ON_ERROR);
        file_put_contents($fileName, $data);
    }

}