<?php

declare(strict_types=1);

namespace App\Infrastructure\Basket;

use App\Infrastructure\Discount\DiscountForCategory;
use App\Infrastructure\Discount\DiscountInterface;
use InvalidArgumentException;

final class BasketDiscountItem extends BasketAbstractItem
{
    private DiscountInterface $discount;

    public function __construct(BasketInterface $basket, DiscountInterface $discount)
    {
        $this->discount = $discount;

        parent::__construct($basket, 1);
    }

    public function changeQty(int $qty): void
    {
        if ($qty > 1) {
            throw new InvalidArgumentException(sprintf('Qty for discount cannot be greater than 1'));
        }

        parent::changeQty($qty);
    }

    public function discount(): DiscountInterface
    {
        return $this->discount;
    }

    public function id(): string
    {
        return 'discount-'.$this->discount->id();
    }

    public function name(): string
    {
        return '';
    }

    public function calculateValue(): int
    {
        $basket = $this->basket();
        $basketValue = $basket->Value();
        $products = $basket->products();

        $discountValue = $this->discount->value($basket, $products);

        return max(-$basketValue,
                    $discountValue);
    }

    public function jsonSerialize(): array
    {
        return array_merge(parent::jsonSerialize(), [
            'type' => 'discount',
            'id' => $this->discount->id(),
        ]);
    }

}