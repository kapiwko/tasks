<?php

declare(strict_types=1);

namespace App\Infrastructure\Basket;

use App\Infrastructure\Product\ProductInterface;

final class BasketProductItem extends BasketAbstractItem
{
    private ProductInterface $product;

    public function __construct(BasketInterface $basket, int $qty, ProductInterface $product)
    {
        $this->product = $product;

        parent::__construct($basket, $qty);
    }

    public function product(): ProductInterface
    {
        return $this->product;
    }

    public function id(): string
    {
        return 'product-'.$this->product->id();
    }

    public function name(): string
    {
        return $this->product->name();
    }

    public function price(): int
    {
        return $this->product->price();
    }

    public function calculateValue(): int
    {
        return $this->product->value($this->qty());
    }

    public function jsonSerialize(): array
    {
        return array_merge(parent::jsonSerialize(), [
            'type' => 'product',
            'id' => $this->product->id(),
        ]);
    }

}