<?php

declare(strict_types=1);

namespace App\Infrastructure\Basket;

use App\Infrastructure\Discount\DiscountInterface;
use App\Infrastructure\Product\ProductInterface;

interface BasketInterface
{
    public function id(): string;

    public function addProduct(ProductInterface $product, int $qty);

    public function removeProduct(ProductInterface $product, int $qty);

    public function calculateDiscounts(): void;

    public function addDiscount(DiscountInterface $discount);

    public function removeDiscount(DiscountInterface $discount);

    public function val(array $items): int;

    public function productsValue(): int;

    public function discountsValue(): int;

    /**
     * @return BasketAbstractItem[]
     */
    public function items(): array;

    public function products(): array;

    public function discounts(): array;

    public function empty(): bool;

    public function clear(): void;

    public function value(): int;

    public function qty(): int;
}