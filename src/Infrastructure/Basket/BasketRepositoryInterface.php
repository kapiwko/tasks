<?php

declare(strict_types=1);

namespace App\Infrastructure\Basket;

interface BasketRepositoryInterface
{
    public function get(string $id): BasketInterface;

    public function add(BasketInterface $basket): void;
}