<?php

declare(strict_types=1);

namespace App\Infrastructure\Basket;

use InvalidArgumentException;
use JsonSerializable;

abstract class BasketAbstractItem implements JsonSerializable
{
    private BasketInterface $basket;
    private int $qty = 0;
    private int $value = 0;

    public function __construct(BasketInterface $basket, int $qty)
    {
        $this->basket = $basket;
        $this->changeQty($qty);
    }

    protected function basket(): BasketInterface
    {
        return $this->basket;
    }

    public function changeQty(int $qty): void
    {
        if ($qty < 0) {
            throw new InvalidArgumentException(sprintf('Qty cannot be negative'));
        }
        $this->qty = $qty;
        $this->value = $this->calculateValue() ;
    }

    final public function qty(): int
    {
        return $this->qty;
    }

    abstract public function id(): string;

    abstract public function name(): string;

    abstract public function calculateValue(): int;

    final public function value(): int
    {
        return $this->value;
    }

    public function jsonSerialize(): array
    {
        return [
            'qty' => $this->qty,
            'value' => $this->value,
        ];
    }

}