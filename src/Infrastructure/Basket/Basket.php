<?php

declare(strict_types=1);

namespace App\Infrastructure\Basket;

use App\Infrastructure\Discount\DiscountInterface;
use App\Infrastructure\Product\ProductInterface;
use InvalidArgumentException;
use JsonSerializable;

final class Basket implements BasketInterface, JsonSerializable
{
    /** @var BasketAbstractItem[] */
    private string $id;
    private array $items = [];   // array of BasketAbstractItem

    private function add(BasketAbstractItem $item)
    {
        $this->items[$item->id()] = $item;
    }


    public function __construct(string $id)
    {
        $this->id = $id;

    }

    public function id(): string
    {
        return $this->id;
    }

    private function remove(BasketAbstractItem $item)
    {
        unset($this->items[$item->id()]);
    }

    public function addProduct(ProductInterface $product, int $qty)
    {
        if ($qty < 0) {
            throw new InvalidArgumentException(sprintf('Qty cannot be negative'));
        }
        $promo_qty = $product->calcPromoQty($qty);  // Taka będzie ilość promocyjna
        $item = new BasketProductItem($this, $qty + $promo_qty, $product);

        if (array_key_exists($item->id(), $this->items)) {
            if ($qty > 0) {
                $current_qty = $this->items[$item->id()]->qty();    // <-- łączna ilość regularna i promocyjna
                $promo_qty = $product->includePromoQty($current_qty);      // Wydobądź ilość promocyjną
                $regular_qty = $current_qty - $promo_qty;

                $regular_qty += $qty;  // Nowa ilość regularna
                $promo_qty = $product->calcPromoQty($regular_qty);  // Nowa ilość prmocyjna

                $this->items[$item->id()]->changeQty($regular_qty + $promo_qty);
            }
        } elseif ($qty > 0) {
            $this->add($item);
            $this->items[$item->id()]->changeQty($qty + $promo_qty);
        }

        $this->calculateDiscounts();
    }

    public function removeProduct(ProductInterface $product, int $qty)
    {   // $qty - ilość produktóœ regularnych do wyjęcia
        if ($qty < 0) {
            throw new InvalidArgumentException(sprintf('Qty cannot be negative'));
        }
        $item = new BasketProductItem($this, $qty, $product);

        $current_qty = $this->items[$item->id()]->qty();    // <-- łączna ilość regularna i promocyjna
        $promo_qty = $product->includePromoQty($current_qty);      // Wydobądź ilość promocyjną
        $regular_qty = $current_qty - $promo_qty;

        if (array_key_exists($item->id(), $this->items)) {
            $regular_qty = max(0, $regular_qty - $qty);       // Nowa ilość regularna
            $promo_qty = $product->calcPromoQty($regular_qty);  // Nowa ilość prmocyjna
            if ($regular_qty + $promo_qty === 0) {
                $this->remove($item);
            } else {
                $this->items[$item->id()]->changeQty($regular_qty + $promo_qty);
            }
        }

        $this->calculateDiscounts();
    }

    public function calculateDiscounts(): void      // przeliczenie zniżek
    {
        foreach ($this->discounts() as $discount) {
            $discount->changeQty(1);
        }
    }

    public function addDiscount(DiscountInterface $discount)
    {
        $item = new BasketDiscountItem($this, $discount);

        if (array_key_exists($item->id(), $this->items)) {
            throw new InvalidArgumentException(sprintf('Discount already exists. (' . $item->id() . ')'));
        }
        $this->add($item);
    }

    public function removeDiscount(DiscountInterface $discount)
    {
        $item = new BasketDiscountItem($this, $discount);
        if (array_key_exists($item->id(), $this->items)) {
            $this->remove($item);
        }
    }

    /**
     * @return BasketAbstractItem[]
     */
    public function items(): array
    {
        return $this->items;
    }

    /**
     * @return BasketProductItem[]
     */
    public function products(): array
    {
        // var_dump($this->items);
        return array_filter(
            $this->items,
            static fn (BasketAbstractItem $item): bool => $item instanceof BasketProductItem
        );
    }

    /**
     * @return BasketDiscountItem[]
     */
    public function discounts(): array
    {
        return array_filter(
            $this->items,
            static fn (BasketAbstractItem $item): bool => $item instanceof BasketDiscountItem,
        );
    }

    public function empty(): bool
    {
        return (count($this->items) === 0);
    }

    public function clear(): void
    {
        $this->items = [];
    }


    public function val(array $items): int
    {
        return array_reduce($items, static fn(int $v, BasketAbstractItem $item): int => $v + $item->value(), 0);
    }

    public function value(): int
    {
        return $this->val($this->items);
        /* NIE KASOWAĆ, powyższy zapis jest równoważny poniższemu
        $sum = 0;

        foreach ($this->items as $item) {
            $sum += $item->value();
        }
        return $sum; */
    }

    public function productsValue(): int
    {
        return $this->val($this->products());
    }

    public function discountsValue(): int
    {
        return $this->val($this->discounts());

    }

    public function qty(): int
    {
        $products = $this->products();

        return count($products);
    }

    public function jsonSerialize(): array
    {
        $data = [];
        $data['id'] = $this->id;
        $data['items'] = $this->items;

        return $data;
    }
}