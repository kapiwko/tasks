<?php

declare(strict_types=1);

namespace App\Infrastructure\Product;


interface ProductInterface
{
    public function id(): string;

    public function name(): string;

    public function category(): string;

    public function price(): int;

    public function value(int $qty): int;

    public function includePromoQty(int $qty): int;

    public function calcPromoQty(int $qty): int;

}