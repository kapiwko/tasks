<?php

declare(strict_types=1);

namespace App\Infrastructure\Product;

final class Product implements ProductInterface
{
    private string $id;
    private string $name;
    private int $price;
    private string $category;

    public function __construct(string $id, string $name, int $price, string $category)
    {
        $this->id = $id;
        $this->name = $name;
        $this->price = $price;
        $this->category = $category;
    }

    public function id(): string
    {
        return $this->id;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function category(): string
    {
        return $this->category;
    }

    public function price(): int
    {
        return $this->price;
    }

    public function value(int $qty): int
    {
        return $this->price * $qty;
    }

    public function calcPromoQty(int $qty): int
    {
        return 0;
    }

    public function includePromoQty(int $qty): int
    {
        return 0;
    }

}
