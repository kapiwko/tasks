<?php

declare(strict_types=1);

namespace App\Infrastructure\Product;

final class PromotionalProduct implements ProductInterface
{
    private Product $product;
    private int $number;
    private int $promoPrice;  // Cena promocyjna

    public function __construct(Product $product, int $number = 0, int $promoPrice = 1)
    {
        $this->product = $product;
        $this->number = $number;    // Co który produkt w promocji
        $this->promoPrice = $promoPrice;      // Cena promocyjna
    }

    public function id():string
    {
        return $this->product->id();
    }

    public function name():string
    {
        return $this->product->name();
    }

    public function category():string
    {
        return $this->product->category();
    }

    public function price():int
    {   // cena regularna
        return $this->product->price();
    }

    public function promo_num(): int
    {
        return $this->number;
    }

    public function promo_price(): int
    {
        return $this->promoPrice;
    }

    public function value(int $qty):int
    {
        $value = 0;
        if ($this->number === 0) {
            $value = $this->product->price() * $qty;
        }
        else {
            $promo_qty = $this->includePromoQty($qty);      // Ilość artykułów w promocji
            $regular_qty = $qty - $promo_qty;               // Ilość artykułów w regularne cenie

            $value = ($promo_qty*$this->promoPrice) + ($regular_qty*$this->product->price());
        }
        return $value;
    }

    public function calcPromoQty(int $qty): int
    {   // W <$qty> jest ilość regularna (bez artykułów promocyjnych)
        // Funkcja zwaraca: ilość artykułów promocyjnych na podstawie zadanej ilość artykułów regularnych
        if ($this->number === 0) {   // Brak promocji
            $promo_qty = 0;
        }
        elseif ($this->number === 1) { // Każdy artykuł w promocji - nie dodajemy dodatkowych artykułow do koszyka
            $promo_qty = 0;
        }
        else {
            /* jeśli np. co 3 artykuł jest w promocji, to przy 2 artykułach ($qty=2) należy zwrócić 1
             co oznacza, że dodatkowo 1 artykuł promocyjny należy dodać do koszyka */
            $number = $this->number - 1;
            $promo_qty = (int)floor($qty/$number);    // Licba artykułów w promocji (zakor.w dół)
        }

        return $promo_qty;
    }

    public function includePromoQty(int $qty): int
    {   // W <$qty> jest łączna ilość: regularna i promocyjna
        // Funkcja zwraca: ilość produktów promocyjnych zawartych w <$qty>
        return (int)floor($qty/$this->number);    // Licba artykułów w promocji (zakor.w dół)
    }
}