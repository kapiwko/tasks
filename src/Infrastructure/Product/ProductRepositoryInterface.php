<?php

declare(strict_types=1);

namespace App\Infrastructure\Product;

interface ProductRepositoryInterface
{
    public function isProduct(string $id): bool;

    public function get(string $id): ProductInterface;

    public function add(ProductInterface $product): void;

    public function remove(ProductInterface $product): void;

    public function commit(): void;

}