<?php

declare(strict_types=1);

namespace App\Infrastructure\Product;

final class ProductRepository implements ProductRepositoryInterface
{
    private ?array $products = null;

    private function addProduct(ProductInterface $product): void
    {
        if (array_key_exists($product->id(), $this->products)) {
            throw new \InvalidArgumentException(sprintf('Product id %s already exists', $product->id()));
        }
        $this->products[$product->id()] = $product;
    }

    public static function readCsv(array &$data, string $filename, callable $parseRow): void
    {
        $handle = fopen($filename, 'r');

        $cnt = 0;
        while (($row = fgetcsv($handle, 0, ';')) !== false) {
            if (($cnt++) === 0) {
                continue;
            }

            $data[$row[0]] = $parseRow($row);
        }
        fclose($handle);
    }

    private function readProducts(): array
    {
        $products = [];
        $promotional = [];

        self::readCsv($promotional, 'products_promotional.csv', static function(array $row): array {
            return [
                "id" => $row[0],
                "promo_num" => $row[1],
                "promo_price" => $row[2],
            ];
        });

        self::readCsv($products, 'products.csv', static function(array $row): array {
            return [
                "id" => $row[0],
                "name" => $row[1],
                "price" => (int)$row[2],
                "category" => $row[3],
                "type" => "normal",
                "promo_num" => 0,
                "promo_price" => 0,
            ];
        });

        foreach ($promotional as $id => $row) {
            if (!array_key_exists($id, $products)) {
                throw new \InvalidArgumentException(sprintf('Unkown product id %s', $id));
            }

            $products[$id]["type"] = "promo";
            $products[$id]["promo_num"] = (int)$row["promo_num"];
            $products[$id]["promo_price"] = (int)$row["promo_price"];
        }

        return $products;
    }

    private function makeProduct(string $id, string $type, array $data): void
    {
        switch ($type) {
            case 'normal':
                $product = new Product($id,
                    $data[$id]["name"],
                    $data[$id]["price"],
                    $data[$id]["category"]);
                $this->addProduct($product);
                break;

            case 'promo':
                $product = new Product($id,
                    $data[$id]["name"],
                    $data[$id]["price"],
                    $data[$id]["category"]);
                $promotionalProduct = new PromotionalProduct($product,
                    $data[$id]["promo_num"],
                    $data[$id]["promo_price"]);
                $this->addProduct($promotionalProduct);
                break;
        }
    }

    public function __construct()
    {

        /*
        $this->addProduct(new Product('MA1','Masło 200g', 500, 'mleczne'));
        $this->addProduct(new Product('CH1', 'Chleb', 250, 'pieczywo'));
        $this->addProduct(new Product('ML1', 'Mleko', 300, 'mleczne'));
        $this->addProduct(new PromotionalProduct(new Product('ML2', 'Mleko2', 600, 'mleczne'), 3, 1));
        $this->addProduct(new PromotionalProduct(new Product('ML3', 'Mleko3', 900, 'mleczne'), 2, 1));
        */
    }

    public function isProduct(string $id): bool
    {
        return array_key_exists($id,$this->products);
    }

    public function get(string $id): ProductInterface
    {
        if (null === $this->products) {
            $this->products = [];
            $data = $this->readProducts();
            foreach ($data as $idx => $productRow) {
                $this->makeProduct($idx, $productRow["type"], $data);
            }
        }

        if (array_key_exists($id, $this->products)) {
            return $this->products[$id];
        }

        throw new \InvalidArgumentException(sprintf('Unkown product id %s', $id));
    }

    public function add(ProductInterface $product): void
    {
        $this->addProduct($product);
    }

    public function remove(ProductInterface $product): void
    {
        unset($this->products[$product->id()]);
    }

    public function commit(): void
    {
        // produkty
        $data = [];
        foreach ($this->products as $product) {
            $data[] = [$product->id(), $product->name(), $product->price(), $product->category()];
        }
        $header = ["ID", "Name", "Price", "Category"];
        self::writeCsv('products.csv', $header, $data);

        // produkty promocyjne
        $data = [];
        foreach ($this->products as $product) {
            if ($product instanceof PromotionalProduct) {
                $data[] = [$product->id(), $product->promo_num(), $product->promo_price()];
            }
        }
        $header = ["ID", "promo_num", "promo_price"];
        self::writeCsv('products_promotional.csv', $header, $data);

    }

    public static function writeCsv(string $filename, array $header, array $data): void
    {
        $handle = fopen($filename, 'w');

        fputcsv($handle, $header, ';');

        foreach ($data as $row) {
            fputcsv($handle, $row, ';');
        }

        fclose($handle);
    }
}