<?php

declare(strict_types=1);

namespace App\Infrastructure\Calendar;

use DateTimeInterface;

interface CalendarInterface
{
    public function isWorkingDay(DateTimeInterface $date): bool;
}
