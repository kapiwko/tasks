<?php

declare(strict_types=1);

namespace App\Infrastructure\Calendar;

use DateInterval;
use DateTimeImmutable;
use DateTimeInterface;

class Calendar implements CalendarInterface
{
    private array $dates = [];

    private function generateDatesForYear(int $year): void
    {
        $dates = [
            '01-01',
            '01-06',
            '05-01',
            '05-03',
            '08-15',
            '11-01',
            '11-11',
            '12-25',
            '12-26',
        ];

        $easterBase = new DateTimeImmutable("$year-03-21");
        $days = easter_days($year);
        $easter = $easterBase->add(new DateInterval("P{$days}D"));

        $dates[] = $easter->format('m-d');
        $dates[] = $easter->add(new DateInterval('P1D'))->format('m-d');
        $dates[] = $easter->add(new DateInterval('P49D'))->format('m-d');
        $dates[] = $easter->add(new DateInterval('P60D'))->format('m-d');

        sort($dates);

        $this->dates[$year] = $dates;
    }

    private function getDatesForYear(int $year): array
    {
        if (!array_key_exists($year, $this->dates)) {
            $this->generateDatesForYear($year);
        }

        return $this->dates[$year];
    }

    public function isWorkingDay(DateTimeInterface $date): bool
    {
        if ((int) $date->format('N') > 6) {
            return false;
        }

        $dates = $this->getDatesForYear((int) $date->format('Y'));

        return !in_array($date->format('m-d'), $dates, true);
    }
}
