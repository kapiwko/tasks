<?php

declare(strict_types=1);

namespace App\Infrastructure\DateRangeValue;

use DateTimeImmutable;

final class DateRangeValue implements DateRangeValueInterface
{
    private int $value;
    private ?DateTimeImmutable $from;
    private ?DateTimeImmutable $to;

    public function __construct(int $value = 0, ?DateTimeImmutable $from = null, ?DateTimeImmutable $to = null)
    {
        $this->value = $value;
        $this->from = $from ? $from->setTime(0, 0) : null;
        $this->to = $to ? $to->setTime(0, 0) : null;
    }

    public function value(): int
    {
        return $this->value;
    }

    public function from(): ?DateTimeImmutable
    {
        return $this->from;
    }

    public function to(): ?DateTimeImmutable
    {
        return $this->to;
    }
}
