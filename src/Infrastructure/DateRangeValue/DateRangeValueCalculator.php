<?php

declare(strict_types=1);

namespace App\Infrastructure\DateRangeValue;

use DateInterval;
use DateTimeImmutable;
use RuntimeException;

final class DateRangeValueCalculator implements DateRangeValueCalculatorInterface
{
    /** @var DateRangeValueInterface[] */
    private array $values = [];
    private array $tags = [];

    public function add(DateRangeValueInterface $value, array $tags = []): void
    {
        $from = $value->from();
        $to = $value->to();
        if (null === $to || null === $from || $from <= $to) {
            $this->values[] = $value;
            $this->tags[] = $tags;
        }
    }

    public function get(DateTimeImmutable $day, ?string $tag = null): int
    {
        return $this->calculateForDay($day->setTime(0, 0), array_values($this->values), $tag);
    }

    private function calculateForDay(DateTimeImmutable $day, array $values, ?string $tag = null): int
    {
        $sum = 0;
        /** @var DateRangeValueInterface $value */
        foreach ($values as $key => $value) {
            if (null !== $tag && !in_array($tag, $this->tags[$key], true)) {
                continue;
            }
            $from = $value->from();
            $to = $value->to();
            if (null !== $from && $from > $day) {
                continue;
            }
            if (null !== $to && $to < $day) {
                continue;
            }
            $sum += $value->value();
        }

        return $sum;
    }

    public function getRange(DateTimeImmutable $from, DateTimeImmutable $to, ?string $tag = null): array
    {
        return $this->calculateForRange($from, $to, array_values($this->values), $tag);
    }

    private function calculateForRange(DateTimeImmutable $from, DateTimeImmutable $to, array $values, ?string $tag = null): array
    {
        $dateFrom = $from->setTime(0, 0);
        $dateTo = $to->setTime(0, 0);
        if ($dateTo < $dateFrom) {
            throw new RuntimeException();
        }
        $days = [];
        $day = clone $dateFrom;
        do {
            $days[$day->format('Y-m-d')] = $this->calculateForDay($day, $values, $tag);
            $day = $day->add(new DateInterval('P1D'));
        } while ($day <= $dateTo);

        return $days;
    }

    public function getMax(DateTimeImmutable $from, DateTimeImmutable $to, ?string $tag = null): int
    {
        $values = $this->calculateForRange($from, $to, array_values($this->values), $tag);

        return max($values);
    }

    public function getMin(DateTimeImmutable $from, DateTimeImmutable $to, ?string $tag = null): int
    {
        $values = $this->calculateForRange($from, $to, array_values($this->values), $tag);

        return min($values);
    }
}
