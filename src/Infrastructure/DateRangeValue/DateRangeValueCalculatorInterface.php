<?php

declare(strict_types=1);

namespace App\Infrastructure\DateRangeValue;

use DateTimeImmutable;

interface DateRangeValueCalculatorInterface
{
    /**
     * @param DateRangeValueInterface $value
     * @param string[] $tags
     */
    public function add(DateRangeValueInterface $value, array $tags = []): void;

    public function get(DateTimeImmutable $day, ?string $tag = null): int;

    public function getRange(DateTimeImmutable $from, DateTimeImmutable $to, ?string $tag = null): array;

    public function getMax(DateTimeImmutable $from, DateTimeImmutable $to, ?string $tag = null): int;

    public function getMin(DateTimeImmutable $from, DateTimeImmutable $to, ?string $tag = null): int;
}
