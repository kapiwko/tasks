<?php

declare(strict_types=1);

namespace App\Infrastructure\DateRangeValue;

use DateTimeImmutable;

interface DateRangeValueInterface
{
    public function __construct(int $value = 0, ?DateTimeImmutable $from = null, ?DateTimeImmutable $to = null);

    public function value(): int;

    public function from(): ?DateTimeImmutable;

    public function to(): ?DateTimeImmutable;
}
