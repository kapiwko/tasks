<?php

declare(strict_types=1);

namespace App\Infrastructure\Discount;

use App\Infrastructure\Basket\BasketInterface;

final class AmountDiscount implements DiscountInterface
{
    private int $amountDiscount;
    private string $id;

    public function __construct(string $id, int $amountDiscount)
    {
        $this->id = $id;
        $this->amountDiscount = $amountDiscount;
    }

    public function id(): string
    {
        return $this->id;
    }

    public function amountDiscount(): int
    {
        return $this->amountDiscount;
    }

    public function value(BasketInterface $basket, array $products): int
    {
        return -$this->amountDiscount;
    }
}