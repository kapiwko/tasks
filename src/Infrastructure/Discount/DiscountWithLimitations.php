<?php

declare(strict_types=1);

namespace App\Infrastructure\Discount;

use App\Infrastructure\Basket\BasketInterface;

final class DiscountWithLimitations implements DiscountInterface
{
    private string $id;
    private int $min;
    private ?int $discountLimit;
    private DiscountInterface $secondaryDiscount;

    public function __construct(string $id, int $min, ?int $discountLimit, DiscountInterface $secondaryDiscount)
    {
        $this->id = $id;
        $this->min = $min;                          // minimalna wartość, od któ©ej naliczna jest zniżka
        $this->discountLimit = $discountLimit;      // limit zniżki (max. kwota zniżki)
        $this->secondaryDiscount = $secondaryDiscount;
    }

    public function id(): string
    {
        return $this->id;
    }

    public function min(): int
    {
        return $this->min;
    }

    public function discountLimit(): ?int
    {
        return $this->discountLimit;
    }

    public function secondaryDiscount(): DiscountInterface
    {
        return $this->secondaryDiscount;
    }

    public function value(BasketInterface $basket, array $products): int
    {
        $discountValue = 0;
        $valueProducts = $basket->productsValue();
        if ($valueProducts >= $this->min)   {
            // należy się jakakolwiek zniżka
            $discountValue = -$this->secondaryDiscount->value($basket, $products);   // value() jest ujemne

            if (!($this->discountLimit === null) && ($discountValue > $this->discountLimit))    {
                $discountValue = $this->discountLimit;
            }
        }

        return -$discountValue;
    }
}