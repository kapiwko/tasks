<?php

declare(strict_types=1);

namespace App\Infrastructure\Discount;

use App\Infrastructure\Basket\BasketProductItem;
use App\Infrastructure\Basket\BasketInterface;
use InvalidArgumentException;

final class DiscountForCategory implements DiscountInterface
{
    private string $id;
    private array $categories = [];
    private DiscountInterface $secondaryDiscount;


    public function __construct(string $id, array $categories, DiscountInterface $secondaryDiscount)
    {

        if (count($categories) === 0) {
            throw new InvalidArgumentException(sprintf('No category specified'));
        }

        $this->id = $id;
        $this->categories = $categories;
        $this->secondaryDiscount = $secondaryDiscount;
    }

    public function id(): string
    {
        return $this->id;
    }

    public function categories(): array
    {
        return $this->categories;
    }

    public function secondaryDiscount(): DiscountInterface
    {
        return $this->secondaryDiscount;
    }

    public function value(BasketInterface $basket, array $products): int
    {
        $categories = $this->categories;
        $productsByCategory = array_filter(
            $products,
            static fn (BasketProductItem $item): bool => in_array($item->product()->category(), $categories));

        $discountValue = $this->secondaryDiscount->value($basket, $productsByCategory);

        // ogranicz zniżkę do wysokości produktów z danej kategorii
        $discountMax = 0;
        foreach ($productsByCategory as $item) {
            $discountMax -= $item->calculateValue();
        }

        return max($discountValue, $discountMax);
    }

}