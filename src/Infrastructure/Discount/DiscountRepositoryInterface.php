<?php

declare(strict_types=1);

namespace App\Infrastructure\Discount;

interface DiscountRepositoryInterface
{
    public function isDiscount(string $id): bool;

    public function get(string $id): DiscountInterface;

    public function add(DiscountInterface $discount): void;

    public function remove(DiscountInterface $discount): void;

    public function commit(): void;
}