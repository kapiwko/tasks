<?php

declare(strict_types=1);

namespace App\Infrastructure\Discount;

use App\Infrastructure\Product\ProductRepository;
use App\Infrastructure\Product\PromotionalProduct;

final class DiscountRepository implements DiscountRepositoryInterface
{
    private ?array $discounts = null;

    private array $loop = [];     // zapętlenie (w rekurencji)

    private function addDiscount(DiscountInterface $discount): void
    {
        if (array_key_exists($discount->id(), $this->discounts)) {
            throw new \InvalidArgumentException(sprintf('Discount id %s already exists', $discount->id()));
        }
        $this->discounts[$discount->id()] = $discount;
    }

    private function readDiscounts(): array
    {
        $data = [];

        ProductRepository::readCsv($data, 'discount_percent.csv', static function(array $row): array {
            return [
                "id"   => $row[0],
                "type" => "percent",
                "percent" => (int)$row[1],
            ];
        });

        ProductRepository::readCsv($data, 'discount_amount.csv', static function(array $row): array {
            return [
                "id"    => $row[0],
                "type"  => "amount",
                "amount" => (int)$row[1],
            ];
        });

        ProductRepository::readCsv($data, 'discount_category.csv', static function(array $row): array {
            return [
                "id"        => $row[0],
                "type"      => "category",
                "category"  => explode(',', $row[1]),
                "secondaryDiscount" => $row[2],
            ];
        });

        ProductRepository::readCsv($data, 'discount_limitations.csv', static function(array $row): array {
            return [
                "id"    => $row[0],
                "type"  => "limitations",
                "min"   => (int)$row[1],
                "discountLimit" => $row[2] === "" ? null : (int)$row[2],
                "secondaryDiscount" => $row[3],
            ];
        });

        return $data;
    }


    // f.rekurencyjna
    private function makeDiscount(string $id, string $type, array $data): void
    {
        if (!array_key_exists($id, $this->discounts)) {
            // Zweryfikuj rekurencyjne zapętlenie
            if (array_key_exists($id, $this->loop)) {
                throw new \InvalidArgumentException("looping recursive function");
            } else {
                $this->loop[$id] = "x";
            }

            switch ($type) {
                case 'amount':
                    $amount = $data[$id]["amount"];
                    $this->addDiscount(new AmountDiscount($id, $amount));
                    break;

                case 'percent':
                    $percent = $data[$id]["percent"];
                    $this->addDiscount(new PercentDiscount($id, $percent));
                    break;

                case 'limitations':
                    $secondary_id = $data[$id]["secondaryDiscount"];
                    $secondary_type = $data[$secondary_id]["type"];
                    $this->makeDiscount($secondary_id, $secondary_type, $data);
                    $min = $data[$id]["min"];
                    $discountLimit = $data[$id]["discountLimit"];
                    $this->addDiscount(new DiscountWithLimitations($id, $min, $discountLimit, $this->get($secondary_id)));
                    break;

                case 'category':
                    $secondary_id = $data[$id]["secondaryDiscount"];
                    $secondary_type = $data[$secondary_id]["type"];
                    $this->makeDiscount($secondary_id, $secondary_type, $data);
                    $category = $data[$id]["category"];
                    $this->addDiscount(new DiscountForCategory($id, $category, $this->get($secondary_id)));
                    break;
            }
            $this->loop = [];   // Zniżka utworzona - wyczyść tablicę rekurencyjnych zapętleń
        }
    }

    public function __construct()
    {

        /*
        // Zniżka kwotowa
        $this->addDiscount(new AmountDiscount('Amount 1', 2001));
        $this->addDiscount(new AmountDiscount('Amount 2', 1000));
        $this->addDiscount(new AmountDiscount('Amount 3', 10));

        // Zniżka procentowa
        $this->addDiscount(new PercentDiscount('Discount 1',1));
        $this->addDiscount(new PercentDiscount('Discount 2', 10));

        // DiscountWithLimitations - zniżka procentowa działająca tak jak opisałem ostanio ;)
        $this->addDiscount(new DiscountWithLimitations('Limitations 1', 10000, 600, $this->get('Amount 2')));
        $this->addDiscount(new DiscountWithLimitations('Limitations 2', 1000, null, $this->get('Amount 2')));
        $this->addDiscount(new DiscountWithLimitations('Limitations 3', 0, null, $this->get('Discount 1')));
        $this->addDiscount(new DiscountWithLimitations('Limitations 4', 0, 12, $this->get('Discount 1')));
        $this->addDiscount(new DiscountWithLimitations('Limitations 5', 1000, null, $this->get('Amount 1')));

        // DiscountForMilk - zniżka na produkty mleczne
        $this->addDiscount(new DiscountForCategory('Milk discount 1', ["mleczne"], $this->get('Amount 1')));  // 2001
        $this->addDiscount(new DiscountForCategory('Milk discount 2', ["mleczne"], $this->get('Amount 2')));  // 1000
        $this->addDiscount(new DiscountForCategory('Milk discount 3', ["mleczne"], $this->get('Discount 1'))); // 1%


        // >>>> Połączenie zniżek
        $this->addDiscount(new DiscountForCategory('Milk discount 4', ["mleczne"], $this->get('Discount 2')));  // 10%
        $this->addDiscount(new DiscountWithLimitations('Limitations 6', 4000, null, $this->get('Milk discount 4')));
        $this->addDiscount(new DiscountWithLimitations('Limitations 7', 3600, null, $this->get('Milk discount 4')));
        $this->addDiscount(new DiscountWithLimitations('Limitations 8', 3600, 200, $this->get('Milk discount 4')));
        */

    }

    public function isDiscount(string $id): bool
    {
        return array_key_exists($id, $this->discounts);
    }

    public function get(string $id): DiscountInterface
    {
        if (null === $this->discounts) {
            $this->discounts = [];
            $data = $this->readDiscounts();
            foreach ($data as $idx => $discountRow) {
                $this->makeDiscount($idx, $discountRow["type"], $data);
            }
        }

        if (array_key_exists($id, $this->discounts)) {
            return $this->discounts[$id];
        }

        throw new \InvalidArgumentException(sprintf('Unkown discount id %s', $id));
    }

    public function add(DiscountInterface $discount): void
    {
        $this->addDiscount($discount);
    }

    public function remove(DiscountInterface $discount): void
    {
        unset($this->discounts[$discount->id()]);
    }

    public function commit(): void
    {
        // discount amount
        $data = [];
        foreach ($this->discounts as $discount) {
            if ($discount instanceof AmountDiscount) {
                $data[] = [$discount->id(), $discount->amountDiscount()];
            }
        }
        $header = ["ID", "amount"];
        ProductRepository::writeCsv('discount_amount.csv', $header, $data);

        // discount percent
        $data = [];
        foreach ($this->discounts as $discount) {
            if ($discount instanceof PercentDiscount) {
                $data[] = [$discount->id(), $discount->percentDiscount()];
            }
        }
        $header = ["ID", "percent"];
        ProductRepository::writeCsv('discount_percent.csv', $header, $data);

        // discount category
        $data = [];
        foreach ($this->discounts as $discount) {
            if ($discount instanceof DiscountForCategory) {
                $categories = implode(',', $discount->categories());
                $secondary_id = $discount->secondaryDiscount()->id();
                $data[] = [$discount->id(), $categories, $secondary_id];
            }
        }
        $header = ["ID", "categories", "discount"];
        ProductRepository::writeCsv('discount_category.csv', $header, $data);

        // discount limitations
        $data = [];
        foreach ($this->discounts as $discount) {
            if ($discount instanceof DiscountWithLimitations) {
                $secondary_id = $discount->secondaryDiscount()->id();
                $data[] = [$discount->id(), $discount->min(), $discount->discountLimit(), $secondary_id];
            }
        }
        $header = ["ID", "min", "limit", "discount"];
        ProductRepository::writeCsv('discount_limitations.csv', $header, $data);
    }



}