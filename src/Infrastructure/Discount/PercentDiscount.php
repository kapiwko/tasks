<?php

declare(strict_types=1);

namespace App\Infrastructure\Discount;

use App\Infrastructure\Basket\BasketInterface;

final class PercentDiscount implements DiscountInterface
{
    private int $percentDiscount;
    private string $id;

    public function __construct(string $id, int $percentDiscount=0)
    {
        $this->id = $id;
        $this->percentDiscount = $percentDiscount;
    }

    public function id(): string
    {
        return $this->id;
    }

    public function percentDiscount(): int
    {
        return $this->percentDiscount;
    }

    public function value(BasketInterface $basket, array $products): int
    {
        $val = $basket->val($products);

        return -(int)($val * $this->percentDiscount / 100);
    }

}